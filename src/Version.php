<?php

namespace pszirmai\sentry;

final class Version
{
    public const SDK_IDENTIFIER = 'sentry.php.yii2';
    public const SDK_VERSION = '4.10.0';
}
